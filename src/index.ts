/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */
function random(): number;
function random(flag: boolean): number;
function random(flag: boolean, end: number): number;
function random(flag: boolean, start: number, end: number): number;
// function random(start:number,end:number)
function random(flag?: boolean, min?: number, max?: number) {

    let rand;
    if (flag === undefined) {
        return Math.round(Math.random());
    }
    else if (min === undefined && max === undefined) {
        rand = Math.random()
        return flag ? rand : Math.round(rand);
    }

    else if (min !== undefined && max === undefined) {
        rand = Math.random() * min
        return flag ? rand : Math.round(rand);
    }
    else if (min !== undefined && max !== undefined) {
        rand = Math.random() * (max - min) + min;
        return flag ? rand : Math.round(rand);
    }
}

const num1 = random(); // 0 ~ 1 | integer
const num2 = random(false); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float

console.log({ num1, num2, num3, num4, num5, num6 });

// ---------------------------------------------------------------
